# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Проект ELRforum -- навчальний проект з метою практично засвоїти технології Web-розробки на Java та розробки відповідної аплікації -- класичного веб-форуму.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Запустити і подивитись можна в 3 кроки:

* Стягнути репозиторій (клон або пул) git clone git@bitbucket.org:valentin_maslowskij/elrforum.git forum
* В MySQL створити ДБ elrforum та виконати скріпт \src\main\resources\database\elrforum
* З комманд лайна з папки проекту запустити mvn clean tomcat7:run

Аплікація доступна localhost:8080/forum/

### Who do I talk to? ###

* Repo owner or admin -- valentin_maslowskij
* Other community or team contact -- ELRforum Team