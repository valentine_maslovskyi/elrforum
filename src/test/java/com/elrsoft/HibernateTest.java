package com.elrsoft;

import com.elrsoft.forum.domain.Topic;
import com.elrsoft.forum.domain.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 16.11.14
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
public class HibernateTest {
    private static SessionFactory factory;
    public static void main(String[] args) {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = factory.openSession();
        Transaction tx = null;
        try{

            tx = session.beginTransaction();
            /*
            Перший приклад - простий аггрегат за допомогою Projections
            агрегат з групуванням і обмеженим
            Згенерований SQL:
            select count(*) as y0_ from topic this_ where this_.author=?
             */
            List simpleCountResult = session.createCriteria(Topic.class).setProjection(
                    Projections.rowCount())
                    .add(Restrictions.eq("author.id", 1))
                    .list();
            /*
            в setProjection() стоїть лише один проджекшен, отже
            simpleCountResult -- це список із одного значення - нашого каунта
             */
            Long oneCount = (Long)(simpleCountResult.get(0));
                System.out.println("Кілкість топіків, які створив юзер з ID=1 => " + oneCount);

            tx.commit();

            tx = session.beginTransaction();

            /*
            Другий приклад - аггрегат з групуванням по полю Автор
            в setProjection() стоїть Projections.projectionList(), отже і результатом буде список масивів
            а в масиві буде те, що я в тому проджекшені пододавав

            Згенерований SQL:
            select count(*) as y0_, this_.author as y1_ from topic this_ group by this_.author
            select user0_.id as id1_3_0_, user0_.avatar_usr as avatar_u2_3_0_, user0_.birthday_usr as birthday3_3_0_, user0_.email_usr as email_us4_3_0_, user0_.login_usr as login_us5_3_0_, user0_.nickname_usr as nickname6_3_0_ from user_usr user0_ where user0_.id=?
            select user0_.id as id1_3_0_, user0_.avatar_usr as avatar_u2_3_0_, user0_.birthday_usr as birthday3_3_0_, user0_.email_usr as email_us4_3_0_, user0_.login_usr as login_us5_3_0_, user0_.nickname_usr as nickname6_3_0_ from user_usr user0_ where user0_.id=?
            select user0_.id as id1_3_0_, user0_.avatar_usr as avatar_u2_3_0_, user0_.birthday_usr as birthday3_3_0_, user0_.email_usr as email_us4_3_0_, user0_.login_usr as login_us5_3_0_, user0_.nickname_usr as nickname6_3_0_ from user_usr user0_ where user0_.id=?
            select user0_.id as id1_3_0_, user0_.avatar_usr as avatar_u2_3_0_, user0_.birthday_usr as birthday3_3_0_, user0_.email_usr as email_us4_3_0_, user0_.login_usr as login_us5_3_0_, user0_.nickname_usr as nickname6_3_0_ from user_usr user0_ where user0_.id=?

             */

            List results = session.createCriteria(Topic.class).setProjection(
                    Projections.projectionList()
                            .add(Projections.rowCount())
                            .add(Projections.groupProperty("author"))
                    )
                    .list();

            for (Object result : results) {
                Object[] aggregate = (Object[]) result;
                Long aggregateCount = (Long)aggregate[0];
                User author = (User)aggregate[1];

                System.out.println("У автора " + author.getLogin() + " " + aggregateCount + " публікацій");
            }

            tx.commit();

        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}

