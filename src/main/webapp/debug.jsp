<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset= utf8"
	pageEncoding="utf8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>

<meta http-equiv="Content-Type" content="text/html"  charset="utf-8">
<title>Secure Page</title>
</head>
<body>
	<div id="content">

		<h1>Secure Page</h1>
		<p>This is a protected page. You can get to me if you've been
			remembered, or if you've authenticated this session.</p>
		<p>
			<sec:authorize access="hasRole('admin')">You are a ADMIN!</sec:authorize>
		</p>
		<p>
			<sec:authorize access="hasRole('user')">You are a USER!</sec:authorize>
		</p>
		<p>
			<sec:authorize access="hasRole('guest')">You are a GUEST!</sec:authorize>
		</p>
		<p>
			<sec:authorize access="isAnonymous()">You are an ANONYMOUS AKA GUEST!</sec:authorize>
		</p>
		<h3>Properties obtained using &lt;sec:authentication /&gt; tag</h3>
		<table border="1">
			<tr>
				<th>Tag</th>
				<th>Value</th>
			</tr>
			<tr>
				<td>&lt;sec:authentication property='name' /&gt;</td>
				<td><sec:authentication property="name" /></td>
			</tr>
			<sec:authorize access="isAuthenticated()">
				<tr>
					<td>&lt;sec:authentication property='principal.username' /&gt;</td>
					<td><sec:authentication property="principal.username" /></td>
				</tr>
				<tr>
					<td>&lt;sec:authentication property='principal.enabled' /&gt;</td>
					<td><sec:authentication property="principal.enabled" /></td>
				</tr>
				<tr>
					<td>&lt;sec:authentication
						property='principal.accountNonLocked' /&gt;</td>
					<td><sec:authentication property="principal.accountNonLocked" /></td>
				</tr>
			</sec:authorize>
		</table>

		<p>
			<a href="index.html">index.html is absent</a>
		</p>
		<p>
			<a href="j_spring_security_logout">Logout</a>
		</p>
		<s:url id="HelloWorld" action="HelloWorld" namespace="/example"></s:url>
		<s:url id="ShowUserList" action="ShowUserList" namespace="/example"></s:url>
		<s:url id="homepage" action="homepage" namespace="/"></s:url>
		<p>
			<s:a href="%{HelloWorld}">No link here...</s:a>
		</p>
		<p>
			<s:a href="%{ShowUserList}">User list with Hibernate.</s:a>
			Цей лінк захищено SpringSecurity. Логін для входу: user2 пароль: 123
		</p>
		<p>
			<s:a href="%{homepage}">Homepage...</s:a>
		</p>
		<p>
			<a href="ShowSubjectList.action">Список Рубрик</a>
		</p>
		<p>
			<a href="example/ShowSubjectListById.action">Список Рубрик за ід</a>
		<p>
		<p>
			<a href="ShowPostByTopic.action">Список Post</a>
		<p>
	</div>
</body>
</html>