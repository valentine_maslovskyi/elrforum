<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title><tiles:getAsString name="title"/></title>
    <link href="<s:url value="/css/main.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="header"><tiles:insertAttribute name="header"/></div>
    <div id="content"><tiles:insertAttribute name="content"/></div>
    <div id="footer"><tiles:insertAttribute name="footer"/></div>
</div>
</body></html>