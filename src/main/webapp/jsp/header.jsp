<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="titleImage">
    <s:url id="logoImage" value="/image/forum.jpg" />
	<img src="${logoImage}" border="1" id="foto" align="left" width="75"
		height="75">
</div>

<s:url id="Profile" action="ViewProfile" namespace="/personal"></s:url>
<s:a href="%{Profile}">Мій профіль</s:a>


<!-- <a href="/forum/personal/ViewProfile.action">Мій профіль</a> -->


<div id="titleInfo">ELRforum</div>
<div id="login" align="right">
    <s:url id="Login" value="/spring_security_login" />
    <s:url id="Logout" value="/j_spring_security_logout" />
    <s:url id="CreateUser" action="CreateUser" namespace="/"/>


	<sec:authorize access="isAnonymous()"><s:a href="%{CreateUser}">Sign in</s:a></sec:authorize>
	<sec:authorize access="isAnonymous()"><s:a href="%{Login}">Login</s:a></sec:authorize>
	<sec:authorize access="!isAnonymous()"><s:a href="%{Logout}">Logout</s:a></sec:authorize>

</div>