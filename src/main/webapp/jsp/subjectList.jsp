<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="utf8"%>
<s:div id="historiLink">
	<s:a action="ShowSubjectsAndPosts">ELR Forum</s:a>
	<s:iterator value="masterHistory">
		<s:url id="historyUrl" action="ShowSubjectsAndPosts">
			<s:param name="masterId" value="%{id}" />
		</s:url>
  >> <s:a href="%{historyUrl}">
			<s:property value="title" />
		</s:a>
	</s:iterator>
</s:div>
<s:if test="sizeSubject!=0">


	<table id="table">
		<tr>
			<th>Форум</th>
			<th>Теми</th>
			<th>Публікації</th>
			<th>Остані публікації (дата, автор, тема)</th>
		</tr>
		<s:iterator value="list">
			<s:url id="ShowSubjectsAndPostsUrl" action="ShowSubjectsAndPosts">
				<s:param name="masterId" value="%{id}" />
			</s:url>
			<tr class="tablebody">

				<td id="firstBlag"><s:a href="%{ShowSubjectsAndPostsUrl}">
						<s:property value="title" />
					</s:a></td>
				<td><s:property value="sizeSubject" /></td>
				<td><s:property value="sizeTopic" /></td>
				<td id="firstBlag"><s:date name="created" />, <s:property
						value="author.login" /></td>




			</tr>
			<br>
		</s:iterator>
	</table>
</s:if>
<br>
<br>
<br>
<s:if test="sizeTopic!=0  & sizeSubject!=0">
	<hr>
</s:if>

<s:if test="sizeTopic!=0">

	<table id="table">
		<tr>
			<th>Теми</th>
			<th>Автор</th>
			<th>Публікацій</th>
			<th>Остані публікації (дата, автор)</th>
		</tr>
		<s:iterator value="topicList">
			<tr class="tablebody">
				<td id="firstBlag"><s:a action="ShowPostList.action">
						<s:property value="title" />
						<s:param name="topicId" value="%{id}" />
						<td><s:property value="author.login" /></td>
						<td><s:property value="sizeTopic" /></td>
						<td id="firstBlag"><s:date name="created" />, <s:property
								value="author.login" /></td>
					</s:a></td>
			</tr>
			<br>
		</s:iterator>
	</table>
</s:if>
<s:if test="masterId!=null">
	<s:a action="CreateTopic.action" namespace="post">create topic
	<s:param name="subjectId" value="%{masterId}" />
	</s:a>
</s:if>
