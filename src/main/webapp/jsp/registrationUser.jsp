<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<s:form action="Registration.action" validate="true">
	<s:textfield name="userLogin" label="Login" value="%{userLogin}" />
	<s:password name="password" label="Password" value="%{password}" />
	<s:password name="confirmPassword" label="Confirm password" value="%{confirmPassword}" />
	<s:textfield name="userName" label="Nickname" value="%{userName}" />
	<s:textfield name="email" label="E-mail" value="%{email}" />
	<s:submit />
</s:form>