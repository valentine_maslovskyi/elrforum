<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<h1>Post by Topic</h1>
<table id="table">
	<tr>
		<th>Форум</th>
		<th>Теми</th>
		<th>Публікації</th>
		<th>Остані публікації (дата, автор, тема)</th>
	</tr>
	<s:iterator value="post">
		<tr class="tablebody">
			<td><s:property value="title" /></td>
			<td><s:property value="created" /></td>
			<td><s:property value="content" /></td>
			<td><s:property value="created" /> <s:property
					value="topic.title" /></td>
		</tr>
	</s:iterator>
	<s:a action="CreateTopic.action" namespace="post">create topic
	<s:param name="topicId" value="%{topicId}" />
	</s:a>
</table>
