<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<h1>User list is under construction</h1>
<table border="1">
	<tr>
		<th>Логін</th>
		<th>Нік</th>
		<th>Пошта</th>
		<th>Дата народження</th>
	</tr>
	<s:iterator value="users">
		<tr class="tablebody">
			<td><s:property value="login" /></td>
			<td><s:property value="nickname" /></td>
			<td><s:property value="email" /></td>
			<td><s:property value="birthday" /></td>
		</tr>
	</s:iterator>
</table>

