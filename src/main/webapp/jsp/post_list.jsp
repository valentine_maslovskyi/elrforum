<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="utf8"%>
<s:div id="historiLink">
<<
	<s:a action="ShowSubjectsAndPosts">
		<s:property value="posts.get(0).topic.subject.title" />
		<s:param name="masterId" value="%{posts.get(0).topic.subject.id}" />
	</s:a>
	<< <s:property value="posts.get(0).topic.title" />
</s:div>

<table id="tablePost">
	<tr>
		<th>Користувач</th>
		<th>Пост</th>
		<th>Лайки</th>
	</tr>
	<s:iterator value="posts">
		<tr class="tablebody">
			<td><s:a action="ViewProfile" namespace="personal">
					<s:property value="author.login" />
					<s:param name="login" value="%{author.login}" />
				</s:a></td>
			<td id="contentPost">
				<div id="ti">
					[
					<s:property value="title" />
					]
				</div> <s:property value="content" />
			</td>
			<td>Like: <s:property value="likes" /> <br> Dislikes: <s:property
					value="dislikes" /> <br> <s:date name="created" /></td>
		</tr>
	</s:iterator>
	<s:a action="CreatePost.action" namespace="post">create post
	<s:param name="topicId" value="%{topicId}" />
	</s:a>
</table>