<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<s:form action="saveTopic.action">
	<s:textfield name="topicTitle" label="Topic Title" />
	<s:textfield name="postTitle" label="Post Title" />
	<s:textarea name="postContent" label="Post content" />
	<s:hidden name="subjectId" value="%{subjectId}" />
	<s:set var="login" scope="request">
		<sec:authentication property="name" />
	</s:set>
	<input type="hidden" name="userLogin" value="${login}" />
	<s:submit />
</s:form>
<s:property value="%{userName}" />