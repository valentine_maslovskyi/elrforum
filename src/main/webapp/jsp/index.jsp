<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="s" uri="/struts-tags" %>

	<s:form action="helloWorld">
        <s:set var="login">
            <sec:authentication property="name" />
        </s:set>
        <input type="hidden" name="userLogin" value="${login}" />
		<s:textfield label="What is your name?" name="name" />
		<s:textfield label="What is the date?" name="dateNow" />
		<s:textfield label="Your e-mail" name="email" />
		<s:submit />
	</s:form>
