package com.elrsoft.forum.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.elrsoft.forum.dao.UserManager;
import com.elrsoft.forum.domain.User;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Namespace("/personal")
@Results(value = { @Result(location = "ProfileView", type = "tiles") })
public class UserProfileAction extends ActionSupport {

	private String login;
	private User user;

	@Action("ViewProfile")
	public String viewUserProfile() {
		if (login == null) {
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			login = auth.getName();
		}
		user = new UserManager().getUserByLogin(login);
		System.out.println(user.getAvatar());
		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
