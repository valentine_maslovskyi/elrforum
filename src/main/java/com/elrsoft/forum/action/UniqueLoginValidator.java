package com.elrsoft.forum.action;

import com.elrsoft.forum.dao.UserManager;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 12/17/14
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class UniqueLoginValidator extends FieldValidatorSupport {
    private static final Logger LOGGER = Logger.getLogger(FieldValidatorSupport.class);

    @Override
    public void validate(Object object) throws ValidationException {

        String fieldName = getFieldName();

        String login = (String) this.getFieldValue(fieldName, object);
        boolean isUnique = new UserManager().isUserLoginExists(login);
        if (!isUnique) {
            addFieldError(fieldName, object);
            LOGGER.error("User with login " + login + " already exists.");
        }
    }
}
