/*
 * Copyright 2006 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.elrsoft.forum.action;

import java.util.Date;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.conversion.annotations.Conversion;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

@Conversion()
public class HelloWorldAction extends ActionSupport {
    
    private Date now;
    private String name;
    private String email;
    private String userLogin;

    @TypeConversion(converter = "com.elrsoft.forum.action.DateConverter")
    public void setDateNow(Date now) { this.now = now; }
    public Date getDateNow() { return now; }
   
    public void setName(String name) { this.name = name; }
    public String getName() { return this.name; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

//    @Validations(
//            requiredStrings =
//                    {@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "name", message = "Please enter a name.")},
//            requiredFields =
//                    {@RequiredFieldValidator(type = ValidatorType.SIMPLE, fieldName = "dateNow", message = "Please enter the date.")},
//            emails =
//                    {@EmailValidator(type = ValidatorType.SIMPLE, fieldName = "email", message = "You must enter a value for email.")}
//    )
    public String execute() throws Exception {
        return SUCCESS;
    }
}
