package com.elrsoft.forum.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.elrsoft.forum.dao.PostManager;
import com.elrsoft.forum.domain.Post;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Results(value = { @Result(location = "ShowPostList", type = "tiles") })
public class ShowPostByTopic extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer topicId;
	private List<Post> posts;

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public List<Post> getPosts() {
		return posts;
	}

	@Action("ShowPostList")
	public String showPostList(){
		posts = new PostManager().getPostsByTopic(topicId);
		return SUCCESS;
	}

}
