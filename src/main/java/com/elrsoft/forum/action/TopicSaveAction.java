package com.elrsoft.forum.action;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.hibernate.Session;

import com.elrsoft.forum.dao.HibernateUtil;
import com.elrsoft.forum.dao.SubjectManager;
import com.elrsoft.forum.dao.TopicManager;
import com.elrsoft.forum.dao.UserManager;
import com.elrsoft.forum.domain.Post;
import com.elrsoft.forum.domain.Subject;
import com.elrsoft.forum.domain.Topic;
import com.elrsoft.forum.domain.User;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Namespace("/post")
@Results(value = { @Result(location = "/ShowPostList.action", params = {
		"topicId", "${topicId}", "namespase", "/" }, type = "redirect") })
public class TopicSaveAction extends ActionSupport {
	private String topicTitle;
	private String postTitle;
	private String postContent;
	private Integer subjectId;
	private Integer topicId;
	private String userLogin;
	private User user;
	private Topic topic;
	private Post post;
	private Subject subject;

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(Integer topicId) {
		this.topicId = topicId;
	}

	@Action("saveTopic")
	public String save() {
		System.out.println(topicId + "esgaszDG");
		if (topicId == null) {
			saveTopic();
		} else {
			savePost();
		}

		return SUCCESS;
	}

	private void createDomain() {
		user = new UserManager().getUserByLogin(userLogin);
	}

	private void createSubject() {
		subject = new SubjectManager().getSubjectById(subjectId);
	}

	private void createSubject(Integer id) {
		subject = new SubjectManager().getSubjectById(id);
	}

	private void createTopic() {
		topic = new TopicManager().getTopicById(topicId);
	}

	private void savePost() {
		createDomain();
		createTopic();
		createSubject(topic.getSubject().getId());
		Session s = HibernateUtil.getCurrentSession();
		// Transaction tx = null;
		try {
			s.beginTransaction();
			post = new Post(new Date(), user, topic, postContent, postTitle, 0,
					0);
			s.persist(post);
			s.getTransaction().commit();
		} catch (RuntimeException e) {
			if (s.getTransaction() != null) {
				s.getTransaction().rollback();
				throw e;
			}
		}
	}

	private void saveTopic() {
		createDomain();
		createSubject();
		Session s = HibernateUtil.getCurrentSession();
		// Transaction tx = null;
		try {
			s.beginTransaction();
			topic = new Topic(topicTitle, user, subject, new Date());
			post = new Post(new Date(), user, topic, postContent, postTitle, 0,
					0);
			setTopicId((Integer) s.save(topic));
			s.persist(post);
			s.getTransaction().commit();
		} catch (RuntimeException e) {
			if (s.getTransaction() != null) {
				s.getTransaction().rollback();
				throw e;
			}
		}
	}

	public String getTopicTitle() {
		return topicTitle;
	}

	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

}
