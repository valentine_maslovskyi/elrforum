package com.elrsoft.forum.action;

import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import com.elrsoft.forum.dao.HibernateUtil;
import com.elrsoft.forum.domain.User;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

@ParentPackage("elrforum")
@Results(value = {@Result(location = "/personal/ViewProfile.action", type = "redirect"),
        @Result(name = "input", location = "NewUserForm", type = "tiles")})
public class RegistrationUserAction extends ActionSupport {

    private String userLogin;
    private String password;
    private String confirmPassword;
    private String userName;
    private String email;

    @RequiredStringValidator(
            type = ValidatorType.FIELD,
            fieldName = "userLogin",
            message = "Please enter a name")
    @CustomValidator(
            type = "uniqueLoginValidator",
            fieldName = "userLogin",
            message = "User login is not unique."
    )
    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD,
            fieldName = "password",
            message = " Please enter a password")
    @StringLengthFieldValidator(type = ValidatorType.FIELD,
            fieldName = "password",
            minLength = "6", maxLength = "16",
            message = " ${minLength} and ${maxLength}")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @FieldExpressionValidator(
            fieldName = "confirmPassword",
            expression = "password eq confirmPassword",
            message = "Password not the same as Confirmed")
    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @RequiredStringValidator(
            type = ValidatorType.FIELD,
            fieldName = "userName",
            message = "Please enter a nickname")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @EmailValidator(type = ValidatorType.FIELD,
            fieldName = "email",
            message = "You must enter a value for email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Action("Registration")
    public String finishRegistration() {
        String result = SUCCESS;
        Session s = HibernateUtil.getCurrentSession();
        Query query = s.createSQLQuery(
                "INSERT INTO authority_art "
                        + "(login_art, authority_art) VALUES (:login_art, :authority_art)");
        try {
            password = new StandardPasswordEncoder().encode(password);
            User user = new User(userLogin, password, userName, email);
            s.beginTransaction();
            s.persist(user);
            query.setParameter("login_art", userLogin);
            query.setParameter("authority_art", "user");
            query.executeUpdate();
            s.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            result = INPUT;
            if (s.getTransaction() != null) {
                s.getTransaction().rollback();

            }
        }
        return result;
    }

    @Action("CreateUser")
    @SkipValidation
    public String showRegistration() {

        return INPUT;
    }
}
