package com.elrsoft.forum.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.elrsoft.forum.dao.HibernateUtil;
import com.elrsoft.forum.dao.UserManager;
import com.elrsoft.forum.domain.User;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Namespace("/personal")
@Results(value = {
		@Result(name = "input", location = "EditProfile", type = "tiles"),
		@Result(name = "success", location = "ViewProfile.action", params = {
				"login", "${login}", "namespace", "/personal" }, type = "redirect") })
public class EditProfileAction extends ActionSupport {
	private User user;
	private Integer id;
	private String login;

	// private

	@Action("EditProfile")
	public String showUser() {
		System.out.println(id);
		if (login != null) {
			user = new UserManager().getUserByLogin(login);
		}
		return INPUT;
	}

	@Action("UpdateProfile")
	public String updateProfile() {
		Session sess = HibernateUtil.getCurrentSession();
		Transaction tx = null;
		try {
			tx = sess.beginTransaction();
			User updateUser = new UserManager().getUserByLogin(user.getLogin());
			updateUser.setAvatar(user.getAvatar());
			updateUser.setBirthday(user.getBirthday());
			updateUser.setNickname(user.getNickname());
			updateUser.setLogin(user.getLogin());
			sess.update(updateUser);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		login = user.getLogin();
		return SUCCESS;
	}

	private User getUser(Integer id) {
		user = new UserManager().getUserById(id);
		System.out.println(user.getLogin());
		return user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
