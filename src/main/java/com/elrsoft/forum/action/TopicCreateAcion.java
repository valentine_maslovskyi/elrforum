package com.elrsoft.forum.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Namespace("/post")
@Results(value = { @Result(location = "TopicCreate", type = "tiles") })
public class TopicCreateAcion extends ActionSupport {
	
	private Integer subjectId;

	@Action("CreateTopic")
	public String create() {
		return SUCCESS;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
}
