package com.elrsoft.forum.action;

import com.elrsoft.forum.dao.UserManager;
import com.elrsoft.forum.domain.User;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 11.10.14
 * Time: 0:30
 */

@ParentPackage("elrforum")
@Namespace("/example")
@Results(value = {
        @Result(location = "UserListView", type = "tiles")})
public class ShowUser extends ActionSupport {
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    private List<User> users;

    @Action("ShowUserList")
    public String showUserList() {

        users = new UserManager().getAllUsers();

        return SUCCESS;
    }


}
