package com.elrsoft.forum.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Namespace("/post")
@Results(value = { @Result(location = "PostCreate", type = "tiles") })
public class PostCreateAction extends ActionSupport {

	private String topicId;

	@Action("CreatePost")
	public String create() {

		return SUCCESS;
	}

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

}
