package com.elrsoft.forum.action;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.elrsoft.forum.dao.SubjectManager;
import com.elrsoft.forum.dao.TopicManager;
import com.elrsoft.forum.domain.Subject;
import com.elrsoft.forum.domain.Topic;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("elrforum")
@Results(value = { @Result(location = "SubjectListView", type = "tiles") })
public class SubjectsAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private List<Subject> list;
	private Integer masterId = null;
	private List<Topic> topicList;
	private int sizeSubject;
	private int sizeTopic;
	private List<Subject> masterHistory = new LinkedList<Subject>();
	private Subject masterSubject;

	public Subject getMasterSubject() {
		return masterSubject;
	}

	public List<Subject> getMasterHistory() {
		return masterHistory;
	}

	public List<Subject> getList() {
		return list;
	}

	public int getSizeSubject() {
		return sizeSubject;
	}

	public int getSizeTopic() {
		return sizeTopic;
	}

	public List<Topic> getTopicList() {
		return topicList;
	}

	@Action("ShowSubjectsAndPosts")
	public String showSubjectList() {
		SubjectManager sm = new SubjectManager();
		list = sm.getSubjectByMaster(masterId);
		topicList = new TopicManager().getTopicsBySubject(masterId);
		sizeSubject = list.size();
		sizeTopic = topicList.size();

		if (masterId != null) {
			masterSubject = sm.getSubjectById(masterId);
			collectMasterIds(masterSubject);
			if (masterHistory.size() > 0) {
				Collections.reverse(masterHistory);
			}
		}

		return SUCCESS;
	}

	public Integer getMasterId() {
		return masterId;
	}

	public void setMasterId(Integer masterId) {
		this.masterId = masterId;
	}

	private void collectMasterIds(Subject s) {
		if (s != null) {
			masterHistory.add(s);
			collectMasterIds(s.getMaster());
		}
	}
}
