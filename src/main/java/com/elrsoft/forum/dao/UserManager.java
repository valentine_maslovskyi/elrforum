package com.elrsoft.forum.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import com.elrsoft.forum.domain.User;

/**
 * Created with IntelliJ IDEA. User: vertigo Date: 12/17/13 Time: 5:39 PM To
 * change this template use File | Settings | File Templates.
 */
public class UserManager extends AbstractManager {
	public List<User> getAllUsers() {
		return (List<User>) getAllData(User.class);
	}

	public User getUserById(int id) {
		return (User) getById(User.class, id);
	}

	public User getUserByLogin(String log) {
		return (User) session.createCriteria(User.class).add(
				Restrictions.eq("login", log)).uniqueResult();
	}

    public boolean isUserLoginExists(String login) {
        return session.createCriteria(User.class).add(
                Restrictions.eq("login", login)).uniqueResult() == null;
    }
}
