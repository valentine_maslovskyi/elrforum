package com.elrsoft.forum.dao;

import java.util.List;

import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.elrsoft.forum.domain.Post;

public class PostManager extends AbstractManager {

	public Post getPostById(int postId) {
        return  (Post) getById(Post.class, postId);
	}

	public List<Post> getPostsByAuthor(int authorId) {
		return (List<Post>) session
					.createCriteria(Post.class)
					.add(Restrictions.eq("author.id", authorId)).list();
	}

	public List<Post> getPostsByTopic(Integer topicId) {
		return (List<Post>) session
					.createCriteria(Post.class)
					.add(Restrictions.eq("topic.id", topicId)).list();
	}
}
