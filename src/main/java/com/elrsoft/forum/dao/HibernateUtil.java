package com.elrsoft.forum.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: vertigo
 * Date: 11/19/13
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory;

    private static final Logger logger = Logger.getLogger(HibernateUtil.class);

    private static ThreadLocal<Session> thSession = new ThreadLocal<Session>();

    static {
        SessionFactory sessf = null;
        try {
            sessf = new Configuration().configure().buildSessionFactory(new StandardServiceRegistryBuilder().configure().build());
        } catch (HibernateException e) {
            System.err.println("Помилка Hibernate при ініціалізації SessionFactory");
            logger.error("Помилка Hibernate при ініціалізації SessionFactory");

            e.printStackTrace();
        } catch (Exception e) {
            System.err.println("Інша помилка при ініціалізації SessionFactory");
            logger.error("Інша помилка при ініціалізації SessionFactory");

            e.printStackTrace();
        }
        sessionFactory = sessf;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session getCurrentSession() {
        logger.info("Getting the session object");
        Session sess = thSession.get();
        if (sess == null) {
            sess = getSessionFactory().openSession();
            thSession.set(sess);
            logger.info("New session is created.");
        }
        return sess;
    }

    public static Integer closeCurrentSession() {
        Session sess = thSession.get();
        Integer hash = null;
        if (sess != null) {
            hash = sess.hashCode();
            sess.close();
            thSession.set(null);
        }
        return hash;
    }
}
