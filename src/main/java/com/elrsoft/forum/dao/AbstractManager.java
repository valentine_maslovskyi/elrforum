package com.elrsoft.forum.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created with IntelliJ IDEA. User: vertigo Date: 12/17/13 Time: 5:29 PM
 */
public class AbstractManager {
	private static final Logger LOGGER = Logger
			.getLogger(AbstractManager.class);

	/**
	 * Field session hold open session for current manager
	 */
	protected Session session;

	/**
	 * Default constructor that is opening new session for this manager instance
	 */
	public AbstractManager() {
		session = HibernateUtil.getCurrentSession();
		LOGGER.info("Create manager using session #[" + session.hashCode()
				+ "]");
	}

	@SuppressWarnings("unchecked")
	protected List<? extends Object> getAllData(Class inputClass) {
        return session.createCriteria(inputClass).list();
	}

	protected Object getById(Class inputClass, Integer idParam) {
		return session.createCriteria(inputClass)
				.add(Restrictions.idEq(idParam)).uniqueResult();
	}
}
