package com.elrsoft.forum.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.elrsoft.forum.domain.Subject;

/**
 * Created with IntelliJ IDEA. User: vertigo Date: 16.10.14 Time: 12:59 To
 * change this template use File | Settings | File Templates.
 */
public class SubjectManager extends AbstractManager {
	public List<Subject> getAllSubject() {
		return (List<Subject>) getAllData(Subject.class);
	}

	public Subject getSubjectById(Integer subjectId) {
		return (Subject) getById(Subject.class, subjectId);
	}

	@SuppressWarnings("unchecked")
	public List<Subject> getSubjectByMaster(Integer masterId) {
		return session.createCriteria(Subject.class)
				.add(Restrictions.eqOrIsNull("master.id", masterId)).list();
	}
}
