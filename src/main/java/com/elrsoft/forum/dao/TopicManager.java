package com.elrsoft.forum.dao;

import java.util.List;

import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.elrsoft.forum.domain.Topic;

public class TopicManager extends AbstractManager {

	public Topic getTopicById(int topicId) {
		return (Topic) getById(Topic.class, topicId);
	}

	public List<Topic> getTopicsByAuthor(Integer authorId) {
		return (List<Topic>) session
					.createCriteria(Topic.class)
					.add(Restrictions.eq("author.id", authorId)).list();
	}

	public List<Topic> getTopicsBySubject(Integer subjectId) {
		return (List<Topic>) session
					.createCriteria(Topic.class)
					.add(Restrictions.eq("subject.id", subjectId)).list();
	}
}
