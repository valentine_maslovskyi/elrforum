package com.elrsoft.forum.domain;

import javax.persistence.*;

import java.util.Date;

/**
 * User: vertigo Date: 11.10.14 Time: 0:17
 *
 */
@Entity
@Table(name = "user_usr")
public class User {
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "login_usr", unique = true, nullable = false)
	private String login;

	@Column(name = "password_usr", unique = false, nullable = false)
	private String password;

	@Column(name = "nickname_usr", unique = false, nullable = true)
	private String nickname;

	@Column(name = "email_usr", unique = false, nullable = true)
	private String email;

	@Column(name = "avatar_usr", unique = false, nullable = true)
	private String avatar;

	@Column(name = "birthday_usr", unique = false, nullable = true)
	private Date birthday;

    public User() {}

	public User(String login, String password, String nickname, String email) {
		super();
		this.login = login;
		this.password = password;
		this.nickname = nickname;
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
