package com.elrsoft.forum.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class Post {
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "created")
	private Date created;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "author")
	private User author;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "topic")
	private Topic topic;

	@Column(name = "content", columnDefinition = "TEXT")
	private String content;

	@Column(name = "title")
	private String title;

	@Column(name = "likes")
	private Integer likes;

	@Column(name = "dislikes")
	private Integer dislikes;

	public Post(Date created, User author, Topic topic, String content,
			String title, Integer likes, Integer dislikes) {
		super();
		this.created = created;
		this.author = author;
		this.topic = topic;
		this.content = content;
		this.title = title;
		this.likes = likes;
		this.dislikes = dislikes;
	}

	public Post() {
	}

	// public Post(Date created, User author, Topic topic, String content,
	// String title) {
	// super();
	// this.created = created;
	// this.author = author;
	// this.topic = topic;
	// this.content = content;
	// this.title = title;
	// }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public Integer getDislikes() {
		return dislikes;
	}

	public void setDislikes(Integer dislikes) {
		this.dislikes = dislikes;
	}

}
