-- MySQL dump 10.11
--
-- Host: localhost    Database: elrforum
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authority_art`
--

DROP TABLE IF EXISTS `authority_art`;
CREATE TABLE `authority_art` (
  `login_art` varchar(64) NOT NULL,
  `authority_art` varchar(60) NOT NULL,
  PRIMARY KEY  (`login_art`,`authority_art`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;

--
-- Dumping data for table `authority_art`
--

LOCK TABLES `authority_art` WRITE;
/*!40000 ALTER TABLE `authority_art` DISABLE KEYS */;
INSERT INTO `authority_art` VALUES ('user1','guest'),('user2','user'),('user3','admin'),('vertigo','admin');
/*!40000 ALTER TABLE `authority_art` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL auto_increment,
  `created` datetime NOT NULL,
  `author` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `likes` int(11) NOT NULL default '0',
  `dislikes` int(11) NOT NULL default '0',
  `topic` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'2014-10-30 23:21:53',1,'ЕПIЛОГ','Хто не жив посеред бурi,',0,0,1),(2,'2014-10-29 23:25:27',2,'КАЗКА ПРО ОХА-ЧАРОДIЯ','В тридев’ятiм славнiм царствi,',0,0,1),(3,'2014-10-30 23:26:03',3,'Чи хто правий, чи неправий','Чи хто правий, чи неправий,',0,0,1),(4,'2014-10-29 23:27:22',4,'На зеленому горбочку','На зеленому горбочку',0,0,2),(5,'2014-11-06 10:42:39',1,'Офисный пакет для Android-смартфонов',' и планшетов, позволяющий просматривать, ',0,0,2),(6,'2014-11-03 10:45:30',2,'Android. Розробка під мобільні пристрої','В даний час мобільні пристрої - смартфони і планшети',0,0,2),(7,'2014-10-09 10:47:56',3,'Програма курсу:','Введення в курс. Узгодження режиму роботи.',0,0,2),(8,'2014-11-05 10:49:45',4,'Чому саме комп\'ютерні курси?','Комп’ютерні курси - це найефективніший спосіб освоїти практично будь-яку сферу діяльності,',0,0,3),(9,'2014-11-04 10:51:23',1,'Самонавчання:','Немає можливості оперативно проконсультуватися у компетентного фахівця.',0,0,3),(10,'2014-11-09 10:52:46',2,'Вузи: ','Величезна кількість зайвої непотрібної інформації.',0,0,4),(11,'2014-11-06 10:53:43',3,'Форма навчання:','Загальна тривалість курсу:',0,0,5),(12,'2014-11-04 10:55:52',4,'Тривалість одного заняття','2 академічні години',0,0,5),(13,'2014-08-06 10:56:48',1,'По закінченню курсу видається свідоцтво.','написання індивідуальної програми',0,0,6),(14,'2014-11-07 10:58:20',2,'ЦЕНТР КОМП\'ЮТЕРНОГО НАВЧАННЯ','Наш Навчальний центр',0,0,7),(15,'2014-09-03 04:59:38',3,'У нас безліч комп\'ютерних курсів і дисциплін','програми комп\'ютерних курсів',0,0,7),(16,'2014-11-05 11:01:32',4,'У НАС МАКСИМАЛЬНО ДОСТУПНЕ НАВЧАННЯ ДЛЯ КОЖНОГО:','Ми постійно удосконалюємо наші навчальні програми курсів',0,0,7),(17,'2014-11-24 11:02:07',1,' НАШІ ВИКЛАДАЧІ - КВАЛІФІКОВАНІ ФАХІВЦІ:',' Ви можете знайти на сторінках нашого сайту імена, посади та відгуки про викладачів.',0,0,8),(18,'2014-11-05 11:03:15',2,'МАКСИМАЛЬНА ДОСТУПНІСТЬ МАТЕРІАЛІВ НАВЧАННЯ:','Записавшись на курси, Ви будете навчатися',0,0,8),(19,'2014-11-04 11:06:52',3,'НАВЧАЛЬНІ ГРУПИ','Саме така кількість людей є найбільш оптимальною для ефективного навчання.',0,0,8),(20,'2014-11-10 11:07:53',4,'ПЕРШИЙ УРОК ПРОБНИЙ:','Ще сумніваєтеся, чи потрібні Вам наші комп\'ютерні курси',0,0,8),(21,'2014-11-17 11:08:43',1,'СПРИЯТЛИВА АТМОСФЕРА І КОМФОРТНІ УМОВИ ДЛЯ НАВЧАННЯ','Сучасна комп\'ютерна техніка',0,0,9),(22,'2014-11-25 11:09:53',2,'Графік роботи ','з понеділка по cуботу ',0,0,10),(23,'2014-11-10 11:13:05',3,'Java Література','Співбесіда по Java / J2EE. Java - Відмінна книга для тих',0,0,10),(24,'2014-11-06 11:22:29',4,'Співбесіда по Java / J2EE. Java','Відмінна книга для тих',0,0,10),(25,'2014-05-14 11:23:11',1,'SCJP Sun Certified Programmer for Java 6 Study Guide','Чудова книга для того, щоб почати вивчати Java',0,0,11),(26,'2014-11-04 11:23:49',2,'Фреймворк Spring Framework','необхідний інструмент для розробників додатків на Java',0,0,11),(27,'2014-11-13 11:24:31',3,'Брюс Еккель. Філософія JAVA.','ava не можна зрозуміти, глянувши на неї лише як на колекцію деяких характеристик,',0,0,12),(28,'2014-11-03 11:25:03',4,'Ми запустили проект','Java Web Services',0,0,12),(29,'2014-11-04 11:27:21',1,'Ця система є спеціалізованим додатком','контролювати діяльність',0,0,12),(30,'2014-11-03 11:28:37',2,'Завдання CRM-систем ','створення чітких і прозорих',0,0,13),(31,'2014-10-06 11:29:58',3,'BMS Service Desk ','це інформаційна система для автоматизації служби підтримки ',0,0,14),(32,'2014-11-04 11:31:00',4,'Система BMS Enterprise Platform ','це інфраструктурне рішення, яке дозволяє швидко і комплексно вирішити завдання автоматизації всіх підрозділів великої організації',0,0,14),(33,'2014-07-15 11:31:33',1,'Картограма','відображає у вигляді схематичної карти набір показників (даних), кожне зі значень якого прив’язано до географічного об’єкту.',0,0,15),(34,'2014-11-18 11:32:32',2,'Прогнозування',' одна   з найбільш необхідних, але при цьому одна з найскладніших  задач.',0,0,15),(35,'2014-11-04 11:33:02',3,'Веб додаток Semantic Analyzer','призначений для автоматичного семантичного аналізу текста з метою виявлення змістовних одиниць даного тексту.',0,0,16),(36,'2014-10-15 11:33:36',4,'BMS Business Analysis Platform ','аналітичне програмне забезпечення, яке дозволяє отримувати необхідні статистичні звіти і пропонувати логічні моделі дій для прийняття тактичн',0,0,16);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `author` int(11) default NULL,
  `created` date default NULL,
  `master` int(11) default NULL,
  `status` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'Новини нашого міста',1,'2014-10-23',NULL,NULL),(2,'Розробка ПЗ',2,'2014-10-23',NULL,NULL),(3,'Хоббі',3,'2014-10-23',NULL,NULL),(4,'Події',4,'2014-10-23',1,NULL),(5,'Новини ВУЗів',4,'2014-10-23',1,NULL),(6,'WEB розробка',4,'2014-10-22',2,NULL),(7,'Mobile розробка',4,'2014-10-23',2,NULL),(8,'Олімпіади',4,'2014-10-23',2,NULL),(9,'Книги',1,'2014-10-23',3,NULL),(10,'Музика',2,'2014-10-23',3,NULL),(11,'Фільми',3,'2014-10-23',3,NULL);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `author` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `subject` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` VALUES (1,'Servlets',1,'2014-10-30 23:09:50',1),(2,'JSP and Scriptlet Development',2,'2014-10-29 23:10:13',1),(3,'Action-Based Frameworks ',3,'2014-10-28 23:10:38',2),(4,'Component-Based Frameworks',4,'2014-10-30 23:11:01',2),(5,'The Great Equalizer – Ajax',1,'2014-10-01 23:11:33',2),(6,'Core Components',2,'2014-10-02 23:12:02',3),(7,'Configuration',3,'2014-10-05 23:12:41',3),(8,'Actions',4,'2014-10-09 23:13:16',4),(9,'Single Result',1,'2014-10-07 23:14:15',5),(10,'Multiple Results',2,'2014-10-02 23:14:40',5),(11,'Result Types ',3,'2014-10-30 23:15:21',6),(12,'Request and Form Data',4,'2014-10-23 23:15:49',7),(13,'Accessing Business Services ',1,'2014-10-07 23:16:19',7),(14,'Accessing Data from the Action',2,'2014-10-22 23:16:51',8),(15,'Interceptors',3,'2014-10-27 23:17:16',9),(16,'Configuration',4,'2014-10-01 23:17:44',10);
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_usr`
--

DROP TABLE IF EXISTS `user_usr`;
CREATE TABLE `user_usr` (
  `login_usr` varchar(64) NOT NULL,
  `password_usr` varchar(80) NOT NULL,
  `enabled_usr` bit(1) NOT NULL default '',
  `nickname_usr` varchar(50) default NULL,
  `email_usr` varchar(255) default NULL,
  `avatar_usr` varchar(255) default NULL,
  `birthday_usr` date default NULL,
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;

--
-- Dumping data for table `user_usr`
--

LOCK TABLES `user_usr` WRITE;
/*!40000 ALTER TABLE `user_usr` DISABLE KEYS */;
INSERT INTO `user_usr` VALUES ('user1','6b51cf1e30be5b45da02eac225726fc76a445d806259eb5cb81608e4426451c44ba6f921d8710ed8','',NULL,NULL,NULL,NULL,1),('user2','6b51cf1e30be5b45da02eac225726fc76a445d806259eb5cb81608e4426451c44ba6f921d8710ed8','',NULL,NULL,NULL,NULL,2),('user3','6b51cf1e30be5b45da02eac225726fc76a445d806259eb5cb81608e4426451c44ba6f921d8710ed8','',NULL,NULL,NULL,NULL,3),('vertigo','6b51cf1e30be5b45da02eac225726fc76a445d806259eb5cb81608e4426451c44ba6f921d8710ed8','',NULL,NULL,NULL,NULL,4);
/*!40000 ALTER TABLE `user_usr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 21:58:45
